# import sys,os
# import HTMLTestRunner
# BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.insert(0,BASE_PATH) #add to environment path
# from lib.path import WEBCASEPATH,REPORTPATH
# import unittest
#
#
# def run():
#     suite = unittest.TestSuite()
#     all_cases = unittest.defaultTestLoader.discover(WEBCASEPATH,'test_add_st*.py')
#     # print(all_cases)
#     [suite.addTests(case) for case in all_cases]
#     f = open(os.path.join(REPORTPATH,'report.html'), 'wb')
#     runner = HTMLTestRunner.HTMLTestRunner(stream=f, title='test report',description='Test report')
#     runner.run(suite)
#
# if __name__ == '__main__':
#     run()

import unittest,os
from lib.path import WEBCASEPATH,REPORTPATH
from BeautifulReport import BeautifulReport
from lib.tool import send_report
suite = unittest.TestSuite()
all_cases = unittest.defaultTestLoader.discover(WEBCASEPATH,'test_add_st*.py')#第一个参数：从哪个目录，第二个参数：定义规则
[suite.addTests(case) for case in all_cases]#列表生成式，和上面的注释写法一样
result = BeautifulReport(suite)
result.report(filename='test_report',description='This is the test report',log_path=REPORTPATH)
send_report(os.path.join(REPORTPATH,'test_report.html'))