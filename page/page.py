from lib.pyse import Pyse
from ddt import ddt,data,unpack,file_data
import os,time
from datetime import datetime
from lib.path import DATA,WEBPICTUREPATH
from selenium.webdriver.common.keys import Keys #引入Keys类包


class BasePage(object):
    def __init__(self):
        # self.d = Pyse('firefox')
        self.d = Pyse('chrome')

    def open(self,url):
        self.d.open(url)

    def quit(self):
        self.d.quit()

    def close(self):
        self.d.close()

    @property
    def driver(self):
        return self.d.driver

    def js(self):
        self.d.js('window.scrollTo(0,0);')

    def get_windows_img(self,file_data):
        self.d.get_windows_img(file_data)

class LoginPage(BasePage):
    def username(self,user):
        css = 'css=>#edit-name'
        self.d.type(css, user)

    def password(self,pwd):
        css = 'css=>#edit-pass'
        self.d.type(css, pwd)

    def button(self):
        css = 'css=>#edit-submit'
        self.d.click(css)

    def login_check(self,user):
        css = 'css=>#toolbar-item-user'
        return self.d.wait_and_save_exception(css,user)


class GoToCreatePage(LoginPage):
    def click_Manage_menu(self):
        css = 'css=>#toolbar-item-administration'
        self.d.click(css)
        content_css = 'link_text=>Content'
        self.d.wait_and_save_exception(content_css,'Display Content error')
        self.d.move_to_element(content_css)
        add_content_css = 'link_text=>Add content'
        self.d.wait_and_save_exception(add_content_css,'Display Add Content error')
        self.d.move_to_element(add_content_css)

    def check_standard(self):
        standard_page_css = 'link_text=>Standard Page'
        self.d.wait_and_save_exception(standard_page_css, 'Display standard page error')
        self.d.click(standard_page_css)
        title_css = 'css=>.page-title'
        return self.d.wait_and_save_exception(title_css, 'Display create standard page error')



class Content_list(GoToCreatePage):
    def search_content(self,title):
        css1 = 'css=>#edit-title'
        self.d.type(css1,title)
        css2 ='css=>#edit-submit-content'
        self.d.click(css2)
        # xpath = "xpath=>//table[@class='views-table views-view-table cols-10 responsive-enabled sticky-enabled sticky-table']/tbody"
        # tbody_v = self.d.get_element(xpath)
        # trs = tbody_v.find_elements_by_tag_name('tr')
        # print(trs)

        xpath3 = "xpath=>//a[contains(.,'%s')]" %title
        self.d.wait_and_exception(xpath3)

    def check_publish(self):
        xpath4 = "xpath=>//td[@headers='view-moderation-state-table-column'][contains(.,'Published')]"
        assert self.d.get_text(xpath4) == 'Published'


    def open_page(self,url,name):
        self.d.open(url)
        self.d.get_windows_img(os.path.join(WEBPICTUREPATH,name)+'.jpg')


class Communal(Content_list):
    def check_newsletter(self,index,date_v,time_v):
        css1 = 'css=>#edit-field-mailchimp-send-type'
        if self.d.get_display(css1):
            self.d.select_by_index(css1,index)
            css2 = 'name=>field_send_schedule_date[0][value][date]'
            css3 = 'name=>field_send_schedule_date[0][value][time]'
            if date_v:
                self.d.type(css2,date_v)
            if time_v:
                self.d.type(css3,time_v)

    def url_alias(self,current_time):
        css = 'css=>#edit-field-url-alias-0-value'
        self.d.type(css,current_time)

    def submit(self):
        css1 = 'css=>.dropbutton-arrow'
        self.d.click(css1)
        css2 = 'css=>.moderation-state-needs-review > input[value="Save and Request Review"]'
        self.d.wait_and_exception(css2)
        self.d.click(css2)

    def status_check(self):
        new_url = self.d.get_url()
        # assert self.current_time in new_url
        css1 = "xpath=>//a[@class='btn btn-small btn-secondary'][contains(.,'Edit draft')]"
        self.d.click(css1)
        css2 = 'css=>#edit-title-0-value'
        self.d.wait_and_exception(css2)
        css3 = 'css=>.dropbutton-arrow'
        self.d.click(css3)

    def need_review_approve(self):
        css4 = 'css=>.dropbutton > li > input[value="Save and Approve (this translation)"]'
        self.d.wait_and_exception(css4)
        self.d.click(css4)

    def approve_publish(self):
        css = 'css=>.dropbutton > li > input[value="Save and Publish (this translation)"]'
        self.d.wait_and_exception(css)
        self.d.click(css)

    def bt_submit(self):
        css = 'css=>#edit-submit'
        self.d.click(css)



class Createstandardpage(Communal):
    def title_and_content(self,title,contents):
        css = 'css=>#edit-title-0-value'
        self.d.type(css,title)
        self.d.type(css,'\t ' + contents)

    def content_upload(self):
        # click star icon to upload image or file
        xpath1= "xpath=>//span[@class='cke_button_icon cke_button__media_browser_icon']"
        self.d.click(xpath1)
        time.sleep(3)
        # switch to iframe
        self.d.switch_to_frame_name(2)
        # upload image from library
        xpath4 = "xpath=>(//img[contains(@typeof,'foaf:Image')])[2]"
        self.d.click(xpath4)
        xpath3 = "xpath=>//input[@value='Place']"
        self.d.click(xpath3)

        # switch back to default page
        self.d.switch_to_default_frame()
        time.sleep(2)
        # click embed
        xpath5 = "xpath=>//button[@class='button button--primary js-form-submit form-submit ui-button ui-corner-all ui-widget']"
        self.d.click(xpath5)
        time.sleep(3)

    def type_source(self,contents):
        #click source to add html
        xpath6 = "xpath=>//span[@class='cke_button_label cke_button__source_label'][1]"
        self.d.click(xpath6)
        # time.sleep(2)
        xpath7 = "xpath=>//textarea[@class='cke_source cke_reset cke_enable_context_menu cke_editable cke_editable_themed cke_contents_ltr']"
        self.d.type(xpath7,contents)
        # time.sleep(2)


    def Top_image(self,name):
        css1 = 'css=>#edit-field-standard-page-image-entity-browser-entity-browser-open-modal'
        self.d.click(css1)
        time.sleep(3)
        # switch to iframe, if not call def content_upload() & def type_source(), below code should be self.d.switch_to_frame_name(2)
        self.d.switch_to_frame_name(name)

        # click to upload a local file
        # xpath1 = "xpath=>//a[@href='#'][contains(.,'Upload')]"
        # self.d.click(xpath1)
        # css2 = 'css=>#edit-input-file'
        # self.d.wait_and_exception(css2)
        # self.d.type(css2,os.path.join(DATA,'5.jpg'))
        # name1 ='name=>bundle'
        # self.d.select_by_value(name1,'image')
        # input1 = 'name=>entity[name][0][value]'
        # self.d.type(input1,'image_name')

        # upload image from library
        xpath4 = "xpath=>(//img[contains(@typeof,'foaf:Image')])[1]"
        self.d.click(xpath4)

        xpath3 = "xpath=>//input[@value='Select']"
        self.d.click(xpath3)
        # switch back to default page
        self.d.switch_to_default_frame()
        xpath2 = "xpath=>//input[@class='form-text required']"
        self.d.type(xpath2,'image_name')

    def accordion(self,ac_tile,ac_contents):
        css1 = 'css=>#edit-field-paragraph-accordion-0-subform-field-title-0-value'
        self.d.type(css1,ac_tile)
        self.d.type(css1, '\t\t ' + ac_contents)

    def related_link(self,ex_link,ex_text,in_link='',description='',):
        css3 = 'css=>#edit-field-paragraph-link-0-subform-field-link-0-uri'
        self.d.type(css3, ex_link)
        css4 = 'css=>#edit-field-paragraph-link-0-subform-field-link-0-title'
        self.d.type(css4, ex_text)
        if in_link and description:
            css1= 'css=>#edit-field-paragraph-link-0-subform-field-internal-link-0-target-id'
            self.d.type(css1,in_link)
            css2 = 'css=>#edit-field-paragraph-link-0-subform-field-link-description-0-value'
            self.d.type(css2,description)


    def more_releted_link(self):
        css1 = "css=>#edit-field-paragraph-link-add-more-add-more-button-link"
        self.d.click(css1)
        name1 = 'name=>field_paragraph_link[1][subform][field_internal_link][0][target_id]'
        self.d.wait_and_save_exception(name1,'more_related_link')

    def related_file(self,text,file):
        css1= 'css=>#edit-field-paragraph-file-0-subform-field-file-description-0-value'
        self.d.type(css1,text)
        name1 = 'name=>files[field_paragraph_file_0_subform_field_file_0]'
        self.d.type(name1,file)
        name2 = 'name=>field_paragraph_file_0_subform_field_file_0_remove_button'
        self.d.wait_and_save_exception(name2,'file_fail')

    def more_related_file(self):
        css = 'css=>#field-paragraph-file-file-add-more'
        self.d.click(css)
        name1 = 'name=>field_paragraph_file[1][subform][field_file_description][0][value]'
        self.d.wait_and_save_exception(name1,'more_relete_file')



class CreateNews(Communal):
    def title(self,title):
        css = 'css=>#edit-title-0-value'
        self.d.type(css,title)

    def category(self,value):
        css1 = 'css=>#edit-field-news-category'
        self.d.select_by_visible_text(css1,value)

    def des_and_content(self,description,content):
        css1 = 'css=>#edit-field-news-description-0-value'
        self.d.type(css1,description)
        self.d.type(css1,'\t ' + content)

    def upload_image(self,file):
        css1 = 'css=>#cke_32'
        self.d.click(css1)



class CreateCustomerAdvisory(Communal):
    def title(self,title):
        css = 'css=>#edit-title-0-value'
        self.d.type(css,title)

    def des_and_content(self,description,content):
        css1 = 'css=>#edit-field-advisory-description-0-value'
        self.d.type(css1, description)
        self.d.type(css1,'\t ' + content)

    def des(self,description):
        css1 = 'css=>#edit-field-advisory-description-0-value'
        self.d.type(css1, description)

    def contents(self,content):
        css1 = 'css=>#'


class CreateSchedule(Communal):
    def title(self,title):
        css = 'css=>#edit-title-0-value'
        self.d.type(css,title)

    def schedule_file(self,file):
        css1 = 'name=>files[field_schedule_file_0]'
        self.d.type(css1,file)
        css2 ='name=>field_schedule_file_0_remove_button'
        self.d.wait_and_save_exception(css2,'remove_button')

    def schedule_region(self,index):
        css1 = 'css=>#edit-field-schedule-region'
        self.d.select_by_index(css1,index)

    def schedule_type(self,text):
        css1 = 'css=>#edit-field-schedule-type'
        # self.d.select_by_index(css1,index)
        self.d.select_by_visible_text(css1,text)

    def schedule_category(self,text):
        css1 = 'css=>#edit-field-schedule-category-target-id'
        self.d.type(css1,text)
        css2 = 'css=>#ui-id-1'
        if self.d.get_display(css2):
            self.d.type(css2,Keys.DOWN)
            self.d.type(css2,Keys.ENTER)
            # time.sleep(3)

class CreateCSRNews(Communal):
    def title(self,title):
        css = 'css=>#edit-title-0-value'
        self.d.type(css,title)

    def category(self,value):
        css1 = 'css=>#edit-field-csr-news-category'
        self.d.select_by_visible_text(css1,value)

    def des_and_content(self,description,content):
        css1 = 'css=>#edit-field-csr-news-description-0-value'
        self.d.type(css1,description)
        self.d.type(css1,'\t ' + content)

    def ifsync(self,value):
        css1 ='css=>#edit-field-is-sync-to-news-value'
        if value:
            self.d.click(css1)
            self.d.element_wait_selected(css1)


class Standard(Createstandardpage):
    pass

class News(CreateNews):
    pass

class CustomerAdvisory(CreateCustomerAdvisory):
    pass

class Schedule(CreateSchedule):
    pass

class CSRNews(CreateCSRNews):
    pass

if __name__ == '__main__':
    pass
