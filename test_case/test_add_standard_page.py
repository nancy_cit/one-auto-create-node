from page.page import Standard
import unittest,os
from lib.tool import Tool
from ddt import ddt, file_data
from lib.path import DATA,DOMAIN,LOG_URL,CONTENT_URL,USER,PWD
from datetime import datetime

@ddt
class Create_StandardPage(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.page = Standard()
        # 每次运行前把失败的截图删除
        cls.tool = Tool()
        cls.tool.clear_picture()
        cls.page.open(DOMAIN + LOG_URL)
        cls.page.username(USER)
        cls.page.password(PWD)
        cls.page.button()
        cls.page.login_check(USER)

    @classmethod
    def tearDownClass(cls):
        cls.page.quit()

    @classmethod
    def node_add(cls,type):
        cls.add_url = '/'.join([DOMAIN,'node/add',type])
        return cls.add_url

    def setUp(self):
        pass
    def tearDown(self):
        pass

    @file_data(os.path.join(DATA, 'create_standard_page.yml'))
    def test_create_standard_page(self,**kwargs):
        ''' add standard page '''
        self.current_time = datetime.now().strftime('%Y%m%d%H%M%S')
        self.page.open_page(self.node_add(kwargs.get('page_type')), kwargs.get('page_type') + '_add_' + self.current_time)
        real_title = ' '.join([kwargs.get('title'),self.current_time])
        self.page.title_and_content(real_title,kwargs.get('content'))
        if kwargs.get('upload') and kwargs.get('source'):
            self.page.content_upload()
            self.page.type_source(kwargs.get('html_content'))
            self.page.Top_image(1)
        else:
            self.page.Top_image(2)
        self.page.accordion(kwargs.get('accordion_title'),kwargs.get('accordion_content'))
        self.page.related_link(kwargs.get('ex_link'),kwargs.get('ex_text'),kwargs.get('in_link'),kwargs.get('description'))
        self.page.more_releted_link()
        self.page.related_file(kwargs.get('file_text'),os.path.join(DATA,kwargs.get('file')))
        self.page.more_related_file()
        self.page.url_alias(self.current_time)
        self.page.submit()
        self.page.status_check()
        self.page.need_review_approve()
        self.page.status_check()
        self.page.approve_publish()
        self.page.open_page(DOMAIN + CONTENT_URL,kwargs.get('page_type')+'_content_list_'+ self.current_time)
        self.page.search_content(real_title)
        self.page.check_publish()
        self.type_name = '-'.join(kwargs.get('page_type').split('_'))
        self.page_url = '/'.join([DOMAIN, self.type_name, self.current_time])
        self.page.open_page(self.page_url,self.type_name + '_detail_' + self.current_time)



if __name__ == '__main__':
    unittest.main()
