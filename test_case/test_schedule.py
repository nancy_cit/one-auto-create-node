from page.page import Schedule
import unittest,os,time
from lib.tool import Tool
from ddt import ddt, file_data
from lib.path import DATA,DOMAIN,LOG_URL,CONTENT_URL,USER,PWD
from datetime import datetime

@ddt
class Create_Schedule(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.page = Schedule()
        # 每次运行前把失败的截图删除
        cls.tool = Tool()
        cls.tool.clear_picture()
        cls.page.open(DOMAIN + LOG_URL)
        cls.page.username(USER)
        cls.page.password(PWD)
        cls.page.button()
        cls.page.login_check(USER)

    @classmethod
    def tearDownClass(cls):
        cls.page.quit()

    @classmethod
    def schedule(cls,type,name):
        cls.schedule_l = [DOMAIN,type,name]
        cls.page_url = '/'.join(cls.schedule_l)
        return cls.page_url

    @classmethod
    def node_add(cls,type):
        cls.add_url = '/'.join([DOMAIN,'node/add',type])
        return cls.add_url

    def setUp(self):
        pass
    def tearDown(self):
        pass

    @file_data(os.path.join(DATA, 'create_schedule.yml'))
    def test_create_schedule(self,**kwargs):
        self.current_time = datetime.now().strftime('%Y%m%d%H%M%S')
        self.page.open_page(self.node_add(kwargs.get('page_type')),kwargs.get('page_type'))
        real_title = ' '.join([kwargs.get('type_name'),kwargs.get('category'),kwargs.get('title'),self.current_time])
        self.page.title(real_title)
        self.page.schedule_file(os.path.join(DATA,kwargs.get('file')))
        self.page.schedule_region(kwargs.get('region'))
        self.page.schedule_type(kwargs.get('type'))
        self.page.schedule_category(kwargs.get('category'))
        if kwargs.get('newsletter'):
            self.page.check_newsletter(kwargs.get('newsletter'),kwargs.get('schedule_date'),kwargs.get('schedule_time'))
        self.page.bt_submit()
        self.page.open_page(DOMAIN + CONTENT_URL,'content_list'+ self.current_time)
        self.page.search_content(real_title)
        # drop-down values: Select a value(0), import(1), export(2), index start from 0
        # self.page.open_page(self.schedule(kwargs.get('type_name'),kwargs.get('type')),kwargs.get('type_name'))




if __name__ == '__main__':
    unittest.main()
