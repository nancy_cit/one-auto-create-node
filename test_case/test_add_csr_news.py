from page.page import CSRNews
import unittest,os
from lib.tool import Tool
from ddt import ddt, file_data
from lib.path import DATA,DOMAIN,LOG_URL,CONTENT_URL,USER,PWD,WEBPICTUREPATH
from datetime import datetime
from lib.log import log

@ddt
class Create_CSR_News(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.page = CSRNews()
        # 每次运行前把失败的截图删除
        cls.tool = Tool()
        cls.tool.clear_picture()
        cls.page.open(DOMAIN + LOG_URL)
        cls.page.username(USER)
        cls.page.password(PWD)
        cls.page.button()
        cls.page.login_check(USER)
        log.debug('login successfully')

    @classmethod
    def tearDownClass(cls):
        cls.page.quit()


    @classmethod
    def node_add(cls,type):
        cls.add_url = '/'.join([DOMAIN,'node/add',type])
        return cls.add_url

    def setUp(self):
        pass
    def tearDown(self):
        pass

    @file_data(os.path.join(DATA, 'create_csr_news.yml'))
    def test_create_news(self,**kwargs):
        ''' add csr news page '''
        self.current_time = datetime.now().strftime('%Y%m%d%H%M%S')
        self.page.open_page(self.node_add(kwargs.get('page_type')), kwargs.get('page_type') + '_add_' + self.current_time)
        log.debug('open add CSR News page')
        real_title = ' '.join([kwargs.get('category'),kwargs.get('title'),self.current_time])
        self.page.title(real_title)
        self.page.category(kwargs.get('category'))
        self.page.des_and_content(kwargs.get('description'),kwargs.get('content'))
        if kwargs.get('newsletter'):
            self.page.check_newsletter(kwargs.get('newsletter'),kwargs.get('schedule_date'),kwargs.get('schedule_time'))
        self.page.url_alias(self.current_time)
        self.page.ifsync(kwargs.get('issync'))
        log.debug(self.current_time)
        # take screenshot of input values
        file_name =kwargs.get('page_type')+ '_inputs_' + self.current_time +'.png'
        self.page.get_windows_img(os.path.join(WEBPICTUREPATH,file_name))
        self.page.submit()
        self.page.status_check()
        self.page.need_review_approve()
        self.page.status_check()
        self.page.approve_publish()
        self.page.open_page(DOMAIN + CONTENT_URL,kwargs.get('page_type')+'_content_list_'+ self.current_time)
        self.page.search_content(real_title)
        self.page.check_publish()
        self.page_url = '/'.join([DOMAIN,kwargs.get('type'),self.current_time])
        self.page.open_page(self.page_url,kwargs.get('type')+'_detail_' + self.current_time)


if __name__ == '__main__':
    unittest.main()
