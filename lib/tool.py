import os,json,csv,yaml,time
from lib.path import WEBPICTUREPATH
from lib.sendmail import SendMail
from lib.path import MAIL_USER_INFO,MAIL_PASSWORD,RECV

class Tool(object):
    def __init__(self):
        self.filelist = os.listdir(WEBPICTUREPATH)

    def error_picture(self):
        picture = []
        for item in self.filelist:
            if item.endswith('.jpg'):
                picture.append((item,))
        return picture

    # 清掉图片
    def clear_picture(self):
        list(map(os.remove, map(lambda file: os.path.join(WEBPICTUREPATH,file), self.filelist)))

def get_Csv(filename):#取得csv中的test case数据
    rows = []
    with open(filename,encoding='utf-8') as f:
        readers = csv.reader(f)
        for row in readers:
            rows.append(row)
    return rows

def get_yml(file):
    f = open(file, encoding='utf-8')
    return json.dumps(yaml.load(f))

# get_yml(os.path.join(FILE_PATH,'sites.yml'))

def send_report(report_file=None):#发送测试报告
    if report_file:#快速导入模块：alt+enter
        title = time.strftime('%Y-%m-%d %H:%M:%S') + 'test report'
        content = '''
        Hi, All
        Refer to attached report file!
        '''
        m = SendMail(MAIL_USER_INFO,MAIL_PASSWORD,RECV,title,content,file=report_file)
        m.send_mail()
